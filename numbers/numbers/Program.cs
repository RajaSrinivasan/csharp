﻿using System;
using System.Collections.Generic;


namespace numbers
{
    class Numbers
    {
        public static void Report(int number,string test,bool success,bool expected)
        {
            Console.Write("{0} test\t{1}\tExpecting {2}; \tGot:", test, number , expected );
            if (success) Console.WriteLine("True");
            else Console.WriteLine("False");
        }
        public static void SelfTest()
        {
            bool status;
            status = IsPrime(9937); Report(9937, "IsPrime", status, false);
            status = IsKaprekar(297); Report(297,"IsKaprekar",status,true);
        }
        
        public static bool IsOdd(int number)
        {
            if ((number % 2) == 1) return true;
            return false;
        }
        public static bool IsPrime(int number)
        {
            List<int> divisors = Divisors(number);
            if (divisors.Count == 2) return true;
            return false;
        }
        public static bool IsPerfect(int number)
        {
            List<int> divisors = Divisors(number);
            int sum_of_divisors = SumOf(divisors);
            if (sum_of_divisors/2 == number)
                return true;
            return false;                 
        }
        public static bool IsKaprekar(int number)
        {
            // yields true for 1, 9, 45, 55, 99, 297, 703
            int numsq = number * number;
            List<int> numsqdigits = Digits(numsq);
            int lefthalflength = numsqdigits.Count / 2;
            int righthalflength = numsqdigits.Count - lefthalflength;
            List<int> lefthalf = numsqdigits.GetRange(0, lefthalflength);
            List<int> righthalf = numsqdigits.GetRange(lefthalflength, righthalflength);
            int lefthalfvalue = ValueOf(lefthalf);
            int righthalfvalue = ValueOf(righthalf);
            Console.WriteLine("{0} {1} {2}", numsq, lefthalfvalue , righthalfvalue);
            if (number == lefthalfvalue+righthalfvalue)
            {
                return true;
            }
            return false;
        }
        public static List<int> Divisors(int number)
        {
            List<int> temp = new List<int> ();
            int sqrtofnum = Convert.ToInt32(Math.Sqrt(number));
            temp.Add(1);
            for (int i = 2; i < sqrtofnum + 1; ++i)
            {
                if ((number % i) == 0)
                {
                    temp.Add(i);
                    if (i != number / i)
                    {
                        temp.Add(number / i);
                    }
                }
            }
            temp.Add(number);
            return temp;
        }
        public static List<int> Digits(int number, int basenum=10)
        {
            List<int> temp = new List<int>();
            if (number == 0)
            {
                temp.Add(0);
            }
            else
            {
                int tempnum = number;
                while (tempnum > 0)
                {
                    temp.Add(tempnum % basenum);
                    tempnum = tempnum / basenum;
                }
            }
            temp.Reverse();
            return temp;
        }
        public static int ValueOf(List<int> digits, int basenum=10)
        {
            int temp = 0;
            foreach (int num in digits)
            {
                temp *= basenum;
                temp += num;
            }
            return temp;
        }

        public static int SumOf(List<int> list)
        {
            int temp = 0;
            foreach (int num in list)
            {
                temp += num;
            }
            return temp;
        }

        public static void show( List<int> nums)
        {
            foreach (int num in nums)
            {
                Console.Write("{0}, ", num);
            }
            Console.WriteLine();
        }
    }

    class Program
    {
        const int MAJOR_VERSION = 0;
        const int MINOR_VERSION = 1;

        static bool check_odd = false;
        static bool check_prime = false;
        static bool check_perfect = false;
        static bool check_kaprekar = false;

        static bool show_digits = false;
        static bool show_divisors = false;

        static bool self_Test = false;

        static int subject1=0;

        static List<int> subjects = new List<int>();

        static void ShowVersion()
        {
            Console.WriteLine("numbers Version {0}-{1}", MAJOR_VERSION, MINOR_VERSION);
        }
 
        static void ShowHelpLine(string sw, string help)
        {
            Console.WriteLine("{0}\t- {1}",sw,help);
        }
        static void ShowHelp()
        {
            Console.WriteLine("numbers [switches]... number");
            ShowHelpLine("-v", "Show Version");
            ShowHelpLine("-h", "Show help");
            ShowHelpLine("-o", "Check if number is odd");
            ShowHelpLine("-p", "Check if number is prime");
            ShowHelpLine("-P", "Check if number is perfect");
            ShowHelpLine("-k", "Check if number is Kaprekar");

            ShowHelpLine("-d", "Show digits of the number");
            ShowHelpLine("-D", "Show divisors of the number");

            ShowHelpLine("-T", "Self Test");
        }
        static void ParseCommandLine(string[] args)
        {
            for (int i = 0; i < args.Length; ++i)
            {
                switch (args[i])
                {
                    case "-o" : check_odd = true;
                        break;
                    case "-p" : check_prime = true;
                        break;
                    case "-P" : check_perfect = true;
                        break;
                    case "-k": check_kaprekar = true;
                        break;
                    case "-d" : show_digits = true;
                        break;
                    case "-D" : show_divisors = true;
                        break;
                    case "-h" : ShowHelp();
                        break;
                    case "-v" : ShowVersion();
                        break;
                    case "-T": self_Test = true;
                        break;
                    default:
                        subject1 = int.Parse(args[i]);
                        subjects.Add( int.Parse(args[i]));
                        break;
                }

            }
        }

        static void Main(string[] args)
        {
            //var num = new Numbers();
            ParseCommandLine(args);
            foreach (int subject in subjects)
            {
                if (check_odd)
                {
                    if (Numbers.IsOdd(subject))
                    {
                        Console.WriteLine("{0} is an odd number", subject);
                    }
                    else
                    {
                        Console.WriteLine("{0} is not an odd number", subject);
                    }
                }

                if (check_prime)
                {
                    if (Numbers.IsPrime(subject))
                    {
                        Console.WriteLine("{0} is a prime", subject);
                    }
                    else
                    {
                        Console.WriteLine("{0} is not a prime", subject);
                    }
                }
                if (check_perfect)
                {
                    if (Numbers.IsPerfect(subject))
                    {
                        Console.WriteLine("{0} is a perfect number", subject);
                    }
                    else
                    {
                        Console.WriteLine("{0} is not a perfect number", subject);
                    }
                }
                if (check_kaprekar)
                {
                    if (Numbers.IsKaprekar(subject))
                    {
                        Console.WriteLine("{0} is a Kaprekar number", subject);
                    }
                    else
                    {
                        Console.WriteLine("{0} is not a Kaprekar Number", subject);
                    }
                }
                if (show_digits)
                {
                    Console.WriteLine("Decimal Digits of {0} are", subject);
                    List<int> actualdigits = Numbers.Digits(subject);

                    Numbers.show(actualdigits);
                    Console.WriteLine("Value is {0}", Numbers.ValueOf(actualdigits));
                }
                if (show_divisors)
                {
                    Console.WriteLine("Divisors of {0} are ", subject);
                    List<int> divisors = Numbers.Divisors(subject);
                    divisors.Sort();
                    Numbers.show(divisors);
                }
            }
            if (self_Test)
            {
                Numbers.SelfTest();
            }
 		}
    }
}
