﻿using System;
namespace signal_processing
{
    public struct Sample
    {
        public double t;
        public double v;
        public void Show()
        {
            Console.WriteLine("{0} {1}", t, v);
        }
    }

    public abstract class Signal
    {
        protected int frequency;
        protected double amplitude;
        protected double phase;
        public Signal(int _freq, double _amp, double _phase)
        {
            frequency = _freq;
            amplitude = _amp;
            phase = _phase;
        }
        public abstract Wave Generate(int _rate, double _starting, double _duration);
    }
    public class Sinusoid : Signal
    {
        public Sinusoid(int _freq, double _amp, double _phase) : base(_freq,_amp,_phase)
        {
            
        }
        public override Wave Generate(int _rate, double _starting, double _duration)
        {

            Wave temp = new Wave(_rate,_starting,_duration);
            double omega = frequency;
            double t = _starting;
            double gap = 1.0 / (double)_rate;
            double dfreq = (double)frequency;

            for (int i = 0; i < temp.samples.Length ; i++)
            {
                double y = amplitude * Math.Sin(Math.PI * 2.0 * dfreq * t + phase);
                temp.Set(i, t, y);
                t += gap;
            }
            return temp;
        }
    }
    public class Wave
    {
        public Sample[] samples;
        int rate;
        double starting;
        double duration;

        public Wave(int _rate, double _starting, double _duration)
        {
            int numsamples = (int)(_duration * (double)_rate);
            samples = new Sample[numsamples];
            rate = _rate;
            starting = _starting;
            duration = _duration;
        }
        public void Set(int _index, double _t, double _v)
        {
            samples[_index].t = _t;
            samples[_index].v = _v;
        }
        public void Set(int _index, Sample _sample)
        {
            samples[_index] = _sample;
        }
        public Sample Get(int _index)
        {
            return samples[_index];
        }

        public double Delta()
        {
            return 1.0 / (double)rate;
        }
        public double Last()
        {
            return starting + duration - Delta();
        }
        public int Index(double sval)
        {
            for (int i= 0; i < samples.Length; i++)
            {
                if (Math.Abs(samples[i].t - sval) < 0.00000001) return i;
            }
            if (sval < samples[0].t) return -1;
            return int.MaxValue;
        }

        private static void Add (Wave left, Wave sum)
        {

            int rpointer = sum.Index(left.starting);
            for (int i = 0; i < left.samples.Length; i++)
            {
                sum.samples[rpointer].v += left.samples[i].v;
                rpointer++;
            }
        }
        public void Init()
        {
            double delta = Delta();
            double temp = starting;
            for (int i = 0; i < samples.Length; i++)
            {
                samples[i].t = temp;
                samples[i].v = 0.0;
                temp += delta;
            }           
        }

        public Wave Add(Wave right)
        {
            Wave sum;
            if (rate != right.rate)
            {
                return null;
            }
            double delta = Delta();
            double sumstart = Math.Min(starting, right.starting);
            double sumend = Math.Max(Last(), right.Last());
            double sumduration = sumend - sumstart + Delta();
            sum = new Wave(rate, sumstart, sumduration);
            sum.Init();

            Add(this, sum);
            Add(right, sum);
            
            return sum;
        }

        public void Shift(double deltat)
        {
            starting += deltat;
            for (int i=0; i< samples.Length; i++)
            {
                samples[i].t += deltat;
            }
        }

        public void Show()
        {
            foreach (Sample s in samples)
            {
                s.Show();
            }
        }
    }
}
