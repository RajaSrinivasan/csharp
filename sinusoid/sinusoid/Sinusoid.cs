﻿using System;

namespace signal_processing
{
    class Program
    {
        static void Test1()
        {
            Signal signal = new Sinusoid(4,2.0,0.0);
            Wave wave = signal.Generate(100, 0.0, 2.0);

            Signal csignal = new Sinusoid(4, 2.0, Math.PI / 2.0);
            Wave cwave = csignal.Generate(100, 0.0, 2.0);

            Wave sum = wave.Add(cwave);
            Console.WriteLine("Sum of Sine and Cosine");
            sum.Show();
                            
        }
        static void Test2()
        {
            Signal signal1 = new Sinusoid(4, 1.0, 0.0);
            Wave wave1 = signal1.Generate(100, 0.0, 2.0);
            Signal signal2 = new Sinusoid(6, 4.0, 0);
            Wave wave2 = signal2.Generate(100, 0.0, 2.0);
            Console.WriteLine("Wave2 shifted");
            wave2.Shift(2.0);
            wave2.Show();
            Console.WriteLine("Wave1+Wave2");
            Wave sum = wave1.Add(wave2);
            sum.Show();
        }
        static void Test3()
        {
            Signal signal1 = new Sinusoid(4, 1.0, 0.0);
            Wave wave1 = signal1.Generate(100, 0.0, 2.0);
            Signal signal2 = new Sinusoid(9, 3.0, Math.PI/2.0);
            Wave wave2 = signal2.Generate(100, 0.0, 2.0);
            Console.WriteLine("Sin(freq=4) Cos(freq=6)");
            Wave sum = wave1.Add(wave2);
            sum.Show();
        }
        static void Main(string[] args)
        {
            Test3();

        }
    }
}
