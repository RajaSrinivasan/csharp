﻿using System;
using System.IO;

namespace lister
{
    class Program
    {
        static int filesListed = 0;
        static void ListFile(string filename)
        {
            int lineNumber = 0;
            Console.WriteLine("Listing File {1} - {0}", filename, ++filesListed);
            StreamReader file = File.OpenText(filename);
            //filesListed++;
            do
            {
                Console.WriteLine("{0,5:D4} : {1}", ++lineNumber , file.ReadLine());
            } while (!file.EndOfStream);
            file.Close();
        }
        static void Main(string[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                ListFile(args[i]);
            }
        }
    }
}
