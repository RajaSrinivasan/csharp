﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;

namespace search
{
    class Debug
    {
        static public bool verbose = false;
        static public void Print(string message)
        {
            if (verbose)
            {
                Console.WriteLine(message);
            }
        }
    }

    class CommandLine
    {
        public List<string> subjects;
        public string candidate;
        public bool verbose = false;
        public bool regex = false;
        public string filenamefilter;

        void ShowHelp()
        {
            Console.WriteLine("search [switches] candidate files...");
            Console.WriteLine("search the file for the occurrance of words/reg exp.");
            ShowHelpLine("-v", "--verbose", "be verbose");
            ShowHelpLine("-h", "--help", "show help");
            ShowHelpLine("-r","--regex", "search for the regexp");
            ShowHelpLine("-f:filter", "--filenames:filter", "filter filenames");
        }

        void ShowHelpLine(string sw, string longsw, string help)
        {
            Console.Write("{0,-8}", sw);
            Console.Write("{0,-20}", longsw);
            Console.WriteLine("{0}", help);
        }

        public CommandLine(string[] args)
        {
            subjects = new List<string>();
            Parse(args);
        }
        void Parse(string[] args)
        {
            foreach (string arg in args)
            {
                Debug.Print(String.Format("Argument {0}",arg));
                switch (arg)
                {
                    case "-v":
                    case "--verbose":
                        verbose = true;
                        Debug.verbose = true;
                        break;
                    case "-h":
                    case "--help":
                        ShowHelp();
                        break;
                    case "-r":
                    case "--regexp":
                        regex = true;
                        break;
                    default:
                        if (!arg.StartsWith("-"))
                        {
                            if (candidate == null)
                            {
                                candidate = arg;
                            }
                            else
                            {
                                subjects.Add(arg);
                            }
                            break;
                        }

                        if (arg.StartsWith("-f:"))
                        {
                            filenamefilter = arg.Substring(3);
                        }
                        else
                        {
                            if (arg.StartsWith("--filenames:"))
                            {
                                filenamefilter = arg.Substring(12);
                            }
                            else
                            {
                                Console.WriteLine("Unrecognized option {0}",arg);
                            }
                        }


                        break;
                }
            }    
        }
    }



    class Search
    {
        static Regex regex;
        static CommandLine cli;
        static Regex filenameregex;

        static void ShowFileName(string filename)
        {
            Console.Write("----- ");
            Console.WriteLine(Path.GetFullPath(filename));
        }
        static void SearchFileSubject(string subject)
        {
            int lineNumber = 0;
            bool shownfilename = false;
            StreamReader file;
            string filebasename = Path.GetFileName(subject);
            if (filenameregex != null)
            {
                if (!filenameregex.IsMatch(filebasename))
                {
                    Debug.Print(String.Format("Ignoring {0}", filebasename));
                    return;
                }
            }

            try
            {
                file = File.OpenText(subject);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception {0} Opening {1}", e.Message, subject);
                return;
            }
            do
            {
                string line = file.ReadLine();
                lineNumber++;
                if (cli.regex)
                {
                    if (regex.IsMatch(line))
                    {
                        if (!shownfilename)
                        {
                            ShowFileName(subject);
                            shownfilename = true;
                        }
                        Console.WriteLine("{0,5:D4} : {1}", lineNumber, line);
                    }
                }
                else
                {
                    if (line.IndexOf(cli.candidate,StringComparison.CurrentCulture) > 0)
                    {
                        if (!shownfilename)
                        {
                            ShowFileName(subject);
                            shownfilename = true;
                        }
                        Console.WriteLine("{0,5:D4} : {1}", lineNumber, line);
                    }
                }
            } while (!file.EndOfStream);
            file.Close();
        }

        static void SearchSubject(string subject)
        {
            FileAttributes fat = System.IO.File.GetAttributes(subject);
            Debug.Print(subject);
            if (fat.HasFlag(FileAttributes.Directory))
            {
                foreach (string subdir in Directory.GetDirectories(subject))
                {
                    SearchSubject(subdir);
                }
                foreach (string file in Directory.GetFiles(subject))
                {
                    SearchFileSubject(file);
                }
            }
            else
            {
                SearchFileSubject(subject);
            }
        }

        static void Main(string[] args)
        {
            cli = new CommandLine(args);
            Debug.verbose = cli.verbose;
            if (cli.regex)
            {
                Debug.Print("Regex search");
                regex = new Regex(cli.candidate, RegexOptions.Compiled);
            }
            else
            {
                Debug.Print(String.Format("Will search for string {0}",cli.candidate));
            }
            if (cli.filenamefilter != null)
            {
                Debug.Print(String.Format("Filename filter set to {0}", cli.filenamefilter));
               filenameregex = new Regex(cli.filenamefilter, RegexOptions.Compiled);
            }
            foreach (string subject in cli.subjects)
            {
                SearchSubject(subject);
            }
        }
    }
}
