﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace digest
{
    class Debug
	{
        static public bool verbose = false;
		static public void Print(string message)
        {
            if (verbose)
            {
                Console.WriteLine(message);
            }
        }
    }
    class CommandLine
    {
        public List<string> subjects ;
        public bool dig_md5, dig_sha1, dig_sha256, dig_sha384, dig_sha512 = false;
        public CommandLine(string[] args)
        {
			subjects = new List<string>();
            Parse(args);
        }
        void ShowHelp()
        {
            Console.WriteLine("digest [switches] args");
            Console.WriteLine("generates digests of the supplied args");
            ShowHelpLine("-v","--verbose","be verbose");
            ShowHelpLine("-h","--help","show help");
            ShowHelpLine("-m","--md5","generate md5 digest");
            ShowHelpLine("-s1","--sha1","generate sha1 digest");
            ShowHelpLine("-s","--sha","generate sha256 digest");
            ShowHelpLine("-s256","--sha256","generate sha256 digest");
            ShowHelpLine("-s384", "--sha384", "generate sha384 digest");
            ShowHelpLine("-s512", "--sh512", "generate sha512 digest");
        }
        void ShowHelpLine(string sw, string longsw, string help)
        {
            Console.Write("{0,-8}", sw) ;
            Console.Write("{0,-20}", longsw);
            Console.WriteLine("{0}", help);
        }
        void Parse(string[] args)
        {
            foreach (string arg in args)
            {
                switch (arg)
                {
                    case "-h" :
                    case "--help":
                        ShowHelp();
                        break;
                    case "-v":
                    case "--verbose":
                        Debug.verbose = true;
                        break;
                    case "-m":
                    case "--md5":
                        dig_md5 = true;
                        break;
                    case "-s1":
                    case "-sha1":
                        dig_sha1 = true;
                        break;
                    case "-s":
                    case "-s256":
                    case "--sha":
                    case "--sha256":
                        dig_sha256 = true;
                        break;
                    case "-s384":
                    case "--sha384":
                        dig_sha384 = true;
                        break;
                    case "-s512":
                    case "--sha512":
                        dig_sha512 = true;
                        break;
   
                    default:
                        if (arg[0].Equals('-'))
                        {
                            Console.WriteLine("{0} is not recognized");
                        }
                        else
                        {
                            Debug.Print(string.Format("Added {0}", arg));
                            subjects.Add(arg);
                        }
                        break;
                }
            }
        }
    }

    class Program
    {
        static CommandLine cli;
        static string Hex(byte[] inp)
        {
			StringBuilder sBuilder = new StringBuilder();

            foreach (byte b in inp)
            {
                sBuilder.Append(b.ToString("x2"));
            }
            return sBuilder.ToString();
        }
        static void Show(string alg, string value)
        {
            Console.WriteLine("{0,-15} : {1}", alg, value);
        }
        static void DigestFile(string filename)
        {
            Console.WriteLine("----- File: {0}", filename);
            byte[] filecontents = File.ReadAllBytes(filename);

            if (cli.dig_md5)
            {
                MD5 hash = MD5.Create();
                string mdigest = Hex( hash.ComputeHash(filecontents) );
                Show("md5" , mdigest);
            }
            if (cli.dig_sha1)
            {
                SHA1 hash = SHA1.Create();
                string mdigest = Hex( hash.ComputeHash(filecontents));
                Show("sha1", mdigest);
            }
            if (cli.dig_sha256)
            {
                SHA256 hash = SHA256.Create();
                string mdigest = Hex(hash.ComputeHash(filecontents));
                Show("sha256", mdigest);
            }
            if (cli.dig_sha384)
            {
                SHA384 hash = SHA384.Create();
                string mdigest = Hex(hash.ComputeHash(filecontents));
                Show("sha384", mdigest);
            }
            if (cli.dig_sha512)
            {
                SHA512 hash = SHA512.Create();
                string mdigest = Hex(hash.ComputeHash(filecontents));
                Show("sha512", mdigest);
            }
        }

        static void DigestDirectory(string dir)
        {
            Console.WriteLine("----- Directory: {0}", dir);
            string[] subdirs = Directory.GetDirectories(dir);
            foreach (string subdir in subdirs)
            {
                DigestDirectory(subdir);
            }
            string[] files = Directory.GetFiles(dir);
            foreach (string file in files)
            {
                DigestFile(file);
            }
        }

        static void Main(string[] args)
        {
            cli = new CommandLine(args);
            FileAttributes fat;
            foreach (string subject in cli.subjects)
            {
                fat = File.GetAttributes(subject);
                if (fat.HasFlag(FileAttributes.Directory))
                {
                    DigestDirectory(subject);
                }
                else
                {
                    DigestFile(subject);
                }
            }
        }

    }
}
